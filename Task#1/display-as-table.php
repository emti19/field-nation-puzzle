<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Field Nation Task 1</title>
  </head>
  <body>

<?php

class CustomArrayObject extends ArrayObject {
    public function displayAsTable() {
        if ($this->count() === 0) {
            echo "<p>No data to display</p>";
            return;
        }
        
        echo "<table border='1'>";
        echo "<tr><th>Key</th><th>Value</th></tr>";
        foreach ($this as $key => $value) {
            echo "<tr><td>$key</td><td>$value</td></tr>";
        }
        echo "</table>";
    }
}

// Instantiating an instance of the CustomArrayObject class
$customArray = new CustomArrayObject();

// Setting up some keys and values for the object
$customArray['Company Name'] = 'Field Nation';
$customArray['Headquarter'] = 'Minnesota';
$customArray['Phone'] = '+1877-573-4353';

// Calling the object's displayAsTable() function to display data as an HTML table
$customArray->displayAsTable();

?>

</body>

</html>