document.addEventListener("DOMContentLoaded", function() { 
    const square = document.getElementById("animatedSquare");
    
    let x = 0;
    let y = 50;
    
    // Initial speed of the square in pixels per second
    let speedX = 10;
    let speedY = 10;
 
    const animateSquare= ()=> {
        // Update the position based on the current speed
        x += speedX;
        y += speedY;

        // Check if the square hits or goes over the right or left edge of the window
        if (x >= window.innerWidth - 100 || x <= 0) {
            // If so, reverse the direction on the X-axis
            speedX = -speedX;
        }

        // Check if the square hits or goes over the bottom or top edge of the window
        if (y >= window.innerHeight - 100 || y <= 0) {
            // If so, reverse the direction on the Y-axis
            speedY = -speedY;
        }

        // Update the square's position on the page
        square.style.left = x + "px";
        square.style.top = y + "px";
    }

    // Set up an interval to call the animateSquare function every second (1000 milliseconds)
    setInterval(animateSquare, 1000);
});
