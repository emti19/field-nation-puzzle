-- Field Nation Task 3

SELECT
    u.user_id,
    u.first_name,
    u.last_name,
    AVG(tr.correct) AS average_correct_answers,
    MAX(tr.time_taken) AS most_recent_test_time
FROM
    users u
LEFT JOIN
    test_result tr ON u.user_id = tr.user_id
GROUP BY
    u.user_id, u.first_name, u.last_name;
